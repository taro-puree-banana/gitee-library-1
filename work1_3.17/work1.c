#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
//调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，所有偶数位于数组的后半部分
//arr[i]为奇数，i++；为偶数，跳出循环。arr[j]为偶数，j--；为奇数，跳出循环。arr[i]与arr[j]进行交换，当i与j相遇后，停止循环
void Han(int arr[], int len)
{
	int i = 0;//数组左下标
	int j = len - 1;//右下标
	while (i < j)
	{
		while (i < j && arr[i] % 2 != 0)
		{
			//arr[i]为奇数
			i++;
		}
		//arr[i]为偶数
		while (i < j && arr[j] % 2 == 0)
		{
			//arr[j]为偶数
			j--;
		}
		//arr[j]为奇数
		
		//交换
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
}

void Printf(int arr[], int len)
{
	for (int i = 0; i < len; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

int main()
{
	int arr[] = {1,2,3,4,5,6,7,8,9};
	int len = sizeof(arr) / sizeof(arr[0]);
	Han(arr, len);
	Printf(arr, len);
	return 0;
}