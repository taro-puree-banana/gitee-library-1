#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
void Han(int n)
{
	if (n < 10)
	{
		printf("%d ", n);
		return ;
	}
	else
	{
		 Han(n / 10);
		printf("%d ", n % 10);
	}
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	Han(n);
	return 0;
}