#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
int Han(int n)
{
	if (n<10)
	{
		return n;
	}
	else
	{
		return n % 10 + Han(n / 10);
	}
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	int m=Han(n);
	printf("%d", m);
	return 0;
}