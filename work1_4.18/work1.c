#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
struct Stu
{
	char name[20];
	int age;
};

//void Stu_fan(struct Stu* p)
//{
//	printf("%s %d", p->name, p->age);
//}
//int main()
//{
//	struct Stu s = { "zhangsan",25 };
//	//printf("%s %d", s.name, s.age);
//	Stu_fan(&s);
//	return 0;
int compare_name(const void* p1, const void* p2)
{
	return strcmp(((struct Stu*)p1)->name, ((struct Stu*)p2)->name);
}
int compare_age(const void* p1, const void* p2)
{
	return ((struct Stu*)p1)->age - ((struct Stu*)p2)->age;
}
int compare_arr(const void* p1, const void* p2)
{
	return *((int*)p1) - *((int*)p2);
}
void struct_printf(struct Stu*s, int sz)
{
	int i = 0;
	for (i = 0; i < 3; i++)
	{
		printf("%s,%d\n", (s+i)->name, (s+i)->age);
	}
}
void arr_printf(int arr1[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr1[i]);
	}
}

void text1()
{
	struct Stu arr[] = { {"zhangsan,16"},{"lisi",25},{"wangwu",17} };
	int sz = sizeof(arr) / sizeof(arr[0]);
	//qsort(arr, sz, sizeof(arr[0]), compare_name);
	qsort(arr, sz, sizeof(arr[0]), compare_age);
	struct_printf(arr, sz);
}
void text2()
{
	int arr1[] = { 0,5,6,7,9,4,3,1,2,8 };
	int sz = sizeof(arr1) / sizeof(arr1[0]);
	qsort(arr1, sz, arr1[0], compare_arr);
	arr_printf(arr1, sz);
}

int main()
{
	//text1();
	text2();
	return 0;
}
