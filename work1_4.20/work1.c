#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
int arr_compare(const void* p1, const void* p2)
{
	return *((int*)p1) - *((int*)p2);
}
void exchang(char* bug1, char* bug2,int width)
{
	int i = 0;
	for (i = 0; i < width; i++)
	{
		char tem = *bug1;
		*bug1 = *bug2;
		*bug2 = tem;
		bug1++;
		bug2++;
	}
}
void my_qsort(void* base, size_t sz, size_t width, int (*com)(const void* p1, const void* p2))
{
	int i = 0;
	for (i = 0; i < sz - 1; i++)
	{
		int j = 0;
		for (j = 0; j < sz - 1 - i; j++)
		{
			if (com((char*)base + j * width, (char*)base + (j + 1) * width)>0)
			{
				exchang((char*)base + j * width, (char*)base + (j + 1) * width, width);
			}
		}
	}
}
void arr_printf(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
}


void text()
{
	int arr[] = { 0,2,6,4,5,8,9,7,1,3 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	my_qsort(arr, sz, sizeof(arr[0]), arr_compare);
	arr_printf(arr, sz);
}
int main()
{
	text();
	return 0;
}