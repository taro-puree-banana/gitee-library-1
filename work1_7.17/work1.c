#include<stdio.h>
struct ListNode
{
	int val;
	struct ListNode*next;
};
typedef struct ListNode ListNode;

ListNode* partition(ListNode* head, int x)
{
	if (head == NULL)
		return head;
	//创建两个带头链表
	ListNode* lesshead, * lesstail;
	ListNode* bighead, * bigtail;
	//创建两个链表的头结点
	lesshead = lesstail = (ListNode*)malloc(sizeof(ListNode));
	bighead = bigtail = (ListNode*)malloc(sizeof(ListNode));
    
	//遍历原链表
	ListNode* pcur = head;
	while (pcur)
	{
		if (pcur->val < x)
		{
			//尾插到小链表中
			lesstail->next = pcur;
			lesstail = lesstail->next;
		}
		else
		{
			//尾插到大链表中
			bigtail->next = pcur;
			bigtail = bigtail->next;
		}
		pcur = pcur->next;
	}
	//修改大链表的尾结点的next指针指向
	bigtail->next = NULL;//若不加这一行代码会出现死循环+next指针初始化

	//小链表的尾结点和大链表的第一个有效节点首尾相连
	lesstail->next = bighead->next;

	ListNode* ret = lesshead->next;
	free(lesshead);
	free(bighead);
	lesshead = bighead = NULL;
	return ret;
}