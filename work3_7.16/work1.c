#include<stdio.h>
struct ListNode
{
	int val;
	struct ListNode* next;
};
typedef struct ListNode ListNode;

struct ListNode* mergeTwolists(struct ListNode* list1, struct ListNode* list2)
{
	//若原链表为空
	if (list1 == NULL)
		return list2;
	if (list2 == NULL)
		return list1;

	ListNode* l1 = list1;
	ListNode* l2 = list2;

	//创建新的链表
	ListNode* Newhead, * Newtail;
	Newhead = Newtail = NULL;


	while (l1 && l2)
	{
		if ((l1->val) < (l2->val))
		{
			//l1拿下来尾插
			if (Newhead == NULL)
			{
				Newhead = Newtail = l1;
			}
			else
			{
				Newtail->next = l1;
				Newtail = Newtail->next;
			}
			l1 = l1->next;
		}
		else
		{
			//l2拿下来尾插
			if (Newhead == NULL)
			{
				Newhead = Newtail = l2;
				
			}
			else
			{
				Newtail->next = l2;
				Newtail = Newtail->next;
				
			}
			l2 = l2->next;
		}
	}


	//跳出循环后有两种情况：要么l1先走到空了，要么l2先走到空了
	if (l2)
		Newtail->next = l2;
	if (l1)
		Newtail->next = l1;

	return Newhead;
}