#define _CRT_SECURE_NO_WARNINGS 
#include<stdio.h>
int main()
{
	int a, b, c;
	while (scanf("%d %d %d", &a, &b, &c) != EOF)
		if (a + b > c && a - b < c)//构成三角形的条件
		{
			if ((a == b) && (a != c) || (a == c) && (a != b) || (c == a) && (c != b))
				//构成等腰三角形，但不是等边三角形
				printf("Isosceles triangle!\n");
			 else if (a == b && b == c)
				//构成等边三角形
				printf("Equilateral triangle!\n");
			else
				printf("Ordinary triangle!\n");

		}
		else
		{
			printf("Not a triangle!\n");
		}

	
	return 0;
}
