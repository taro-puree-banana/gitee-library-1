#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

int main()
{
	int arr[] = { 1,2,3,4,5,4,3,2,1 };
	int i = 0;
	int j = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	while ( i < sz)
	{
		for (j = 0; j < sz; j++)
		{
			if (arr[i] == arr[j]&& (i!=j))
			{
				i++;
				break;
			}
		}
		if (j == sz)
			printf("%d", arr[i++]);
	}
	return 0;
}

//只有一个数字出现一次，其他数组都是成对出现的，说明成对出现的数据类似与a^a=0，
// 在成对数据都^后只剩下0和我们要查找的数，这时再查找数^0=查找数，数就可以实现我们的要求

#include <stdio.h>

int Fanc(int arr[], int sz)
{
	int ret = 0;
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		ret ^= arr[i];
	}
	return ret;
}
int main()
{
	int arr[] = { 1,2,3,4,5,1,2,3,4 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int n = Fanc(arr, sz);
	printf("%d\n", n);


	return 0;
}
