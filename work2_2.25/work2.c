#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
//一
//通过n%2再n/2的循环，求出n的二进制中1的个数
int Fanc(unsigned int a)
{
	int count = 0;
	while (a)
	{
		if (a % 2)
		{
			count++;
		}
		a /= 2;
	}
	return count;
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	int b = Fanc(n);
	printf("%d", b);
	return 0;
}

//二
//&：按位与，相同为1，不同为0；通过将整数的二进制序列&1，得出末尾是1还是0，若==1，则末尾为1，若为0，则末尾是0；
// 再将二进制序列右移，依次得出序列中1的个数。
int Fanc(unsigned int a)
{
	int count = 0;
	for (int i = 0; i < 32; i++)
	{
		if ((a >> i) & 1 == 1)
		{
			count++;
		}
	}
	return count;
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	int b = Fanc(n);
	printf("%d", b);
	return 0;
}

//三
//n=n&(n-1)通过这个循环，让n的二进制中最右边的1消失

int Fanc(unsigned int a)
{
	int count = 0;
	while (a)
	{
		
		a = a & (a - 1);
		count++;
	}
	return count;
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	int b = Fanc(n);
	printf("%d", b);
	return 0;
}
