#include<stdio.h>
struct ListNode
{
	int val;
	struct LostNode* next;
};
typedef struct ListNode ListNode;
//创建节点
ListNode* createpoint(int x)
{
	ListNode* node = (ListNode*)malloc(sizeof(ListNode));
	if (node == NULL)
	{
		exit(1);
	}
	node->val = x;
	node->next = NULL;
	return node;
}
//创建连环链表
ListNode* creatcircle(int n)
{
	ListNode* phead = createpoint(1);
	ListNode* ptail = phead;
	for (int i = 2; i <= n; i++)
	{
		ptail->next = createpoint(i);
		ptail = ptail->next;
	}
	ptail->next = phead;
	return ptail;
}

int yefcircle(int n,int m)
{
	ListNode* prev = creatcircle(n);
	ListNode* pcur = prev->next;
	int count = 1;
	while (pcur->next != pcur)
	{
		if (count == m)
		{
			prev->next = pcur->next;
			free(pcur);
			pcur = prev->next;
			count = 1;
		}
		else
		{
			prev = pcur;
			pcur = pcur->next;
			count++;
		}
	}
	//int a=pcur->val;
	//free(pcur);
	//return a;
	return pcur->val;
}