#include"seqlist.h"
//void print(SL s2)
//{
//	int i = 0;
//	for (i = 0; i < s2.size; i++)
//	{
//		printf("%d ", s2.arr[i]);
//	}
//	printf("\n");
//}
//顺序表初始化
void SLInit(SL* s)
{
	s->arr = NULL;
	s->size = s->capacity = 0;
}
//顺序表的销毁
void SLDestroy(SL* ps)
{
	if (ps->arr)
	{
		free(ps->arr);
	}
	ps->arr = NULL;
	ps->size = ps->capacity = 0;
}
void SLCheckcapacity(SL* ps)//扩容
{
	if (ps->size == ps->capacity)
	{
		int newcapacity = ps->capacity == 0 ? 4 : 2 * ps->capacity;
		SLDateType* tem = realloc(ps->arr, newcapacity * sizeof(SLDateType));
		if (tem == NULL)
		{
			perror("realloc fail!");
			exit(1);
		}
		ps->arr = tem;
		ps->capacity = newcapacity;
	}
}
//尾插
void SLPushback(SL* ps, SLDateType x)
{
	assert(ps);//为假就报错，等价于assert(ps!=NULL)
	SLCheckcapacity(ps);
	ps->arr[ps->size] = x;
	++ps->size;
	//ps->arr[ps->size++]=x;
}
//头插
void SLPushfront(SL* ps, SLDateType x)
{
	assert(ps);
	SLCheckcapacity(ps);
	int i = 0;
	for(i=ps->size;i>0;i--)
	{
		ps->arr[i] = ps->arr[i - 1];
	}
	ps->arr[0] = x;
	ps->size++;
}
//尾删
void SLPopback(SL* ps)
{
	assert(ps);
	assert(ps->size);
	//SLCheckcapacity(ps);
	ps->size--;
}
//头删
void SLPopfront(SL* ps)
{
	assert(ps);
	assert(ps->size);
	int i = 0;
	for (i = 0; i < ps->size - 1; i++)
	{
		ps->arr[i] = ps->arr[i + 1];
	}
	ps->size--;
}
//在指定位置之前插入数据
void SLInsert(SL* ps,int pos,SLDateType x)
{
	//pos代表指定位置的下标,范围有限制
	assert(ps);
	assert(pos>=0&&pos<=ps->size);
	SLCheckcapacity(ps);
	int i = 0;
	for (i = ps->size; i >pos; i--)
	{
		ps->arr[i+1] = ps->arr[i]; 
	}
	ps->arr[pos] = x;
	ps->size++;
}
//删除指定位置的数据
void SLdelete(SL* ps, int pos)
{
	assert(ps);
	assert(ps->size);
	int i = 0;
	for (i = pos; i < ps->size - 1; i++)
	{
		ps->arr[i] = ps->arr[i + 1];
	}
	ps->size--;
}