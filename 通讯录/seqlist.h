#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include"contact.h"
//定义顺序表的结构

//静态顺序表
//struct Seqlist
//{
//	int arr[100];
//	int size;
//};

//动态顺序表
typedef person SLDateType;
typedef struct Seqlist
{
	SLDateType* arr;
	int size;//有效的数据个数
	int capacity;//空间大小
}SL;
void print(s1);
//顺序表初始化
void SLInit(SL* ps);
//顺序表的销毁
void SLDestroy(SL* ps);
//尾插
void SLPushback(SL* ps, SLDateType x);
//头插
void SLPushfront(SL* ps, SLDateType x);
//尾删
void SLPopback(SL* ps);
//头删
void SLPopfront(SL* ps);
//在指定位置之前插入数据
void SLInsert(SL* ps,int pos,SLDateType x);
//删除指定位置的数据
void SLdelete(SL* ps, int pos);
