#define _CRT_SECURE_NO_WARNINGS
#include"seqlist.h"
#include"contact.h"
void ContactInit(contact* con)//通讯录初始化
{
	SLInit(con);
}

void ContactDestroy(contact* con)//通讯录的销毁
{
	SLDestroy(con);
}
void Contactshow(contact* con)//展示通讯录数据
{
	printf("姓名 性别 年龄 电话 住址\n");
	int i = 0;
	for (i = 0; i < con->size; i++)
	{
		printf("%s   %s   %d   %s    %s\n", 
			con->arr[i].name,
			con->arr[i].gender,
			con->arr[i].age,
			con->arr[i].tel,
			con->arr[i].addr);
	}
}
void ContactAdd(contact* con)
{
	person son;
	printf("请输入要添加的联系人的姓名：\n");
	scanf("%s", son.name);
	printf("请输入要添加的联系人的性别：\n");
	scanf("%s", son.gender);
	printf("请输入要添加的联系人的年龄：\n");
	scanf("%d", &son.age);
	printf("请输入要添加的联系人的电话：\n");
	scanf("%s", son.tel);
	printf("请输入要添加的联系人的地址：\n");
	scanf("%s", son.addr);
	SLPushback(con, son);//尾插

}
int Find_(contact* con, char name[])
{
	int i = 0;
	for (i = 0; i < con->size; i++)
	{
		if (0 == strcmp(con->arr[i].name, name))
		{
			return i;
		}
	}
	return -1;
}
void ContactDel(contact* con)
{
	char name[Name_Max];
	printf("请输入要删除的联系人姓名：\n");
	scanf("%s", name);
	int find = Find_(con, name);
	if (find < 0)
	{
		printf("要删除的联系人数据不存在\n");
	}
	else
	{
		SLdelete(con, find);
		printf("删除成功\n");
	}
}

//修改联系人
void ContactModify(contact* con)
{
	char name[Name_Max];
	printf("请输入要修改的联系人姓名\n");
	scanf("%s", &name);
	int find = Find_(con, name);
	if (find < 0)
	{
		printf("要修改的联系人不存在\n");
	}
	else 
	{
		printf("请输入新的联系人的姓名\n");
		scanf("%s", con->arr[find].name);
		printf("请输入新的联系人的性别\n");
		scanf("%s", con->arr[find].gender);
		printf("请输入新的联系人的年龄\n");
		scanf("%d",& con->arr[find].age);
		printf("请输入新的联系人的电话\n");
		scanf("%s", con->arr[find].tel);
		printf("请输入新的联系人的地址\n");
		scanf("%s", con->arr[find].addr);
		printf("删除成功\n");
	}
}

//通讯录的查找
void ContactFind(contact* con)
{
	char name[Name_Max];
	printf("请输入要查找的联系人姓名\n");
	scanf("%s", name);
	int find = Find_(con, name);
	if (find < 0)
	{
		printf("要查找的联系人数据不存在\n");
		return;
	}
	printf("姓名 性别 年龄 电话 住址\n");
	printf("%s   %s   %d   %s    %s\n",
		con->arr[find].name,
		con->arr[find].gender,
		con->arr[find].age,
		con->arr[find].tel,
		con->arr[find].addr);
}