#include"seqlist.h"

void SLText01()
{
	//SL s1;
	//SLInit(&s1);//初始化
	//SLPushback(&s1, 1);//尾插
	//SLPushback(&s1, 2);
	//SLPushback(&s1, 3);
	//SLPushback(&s1, 4);
	//print(s1);
	/*SLPushfront(&s1, 5);
	SLPushfront(&s1, 6);*/
	//SLPopfront(&s1);//头删
	//print(s1);
	//SLPopfront(&s1);
	//print(s1);
	//SLPopfront(&s1);
	//print(s1);
	/*SLInsert(&s1, 4, 5);
	print(s1);
	SLdelete(&s1, 2);
	print(s1);*/
	//SLDestroy(&s1);//销毁
}
void ContactText02()
{
	contact con;
	ContactInit(&con);//通讯录初始化
	ContactAdd(&con);//添加联系人数据
	ContactAdd(&con);
	ContactModify(&con);
	Contactshow(&con);//显示通讯录
	ContactDestroy(&con);//通讯录销毁
}
void menu()
{
	printf("**************通讯录***************\n");
	printf("*****1.增加联系人  2.删除联系人****\n");
	printf("*****3.修改联系人  4.查找联系人****\n");
	printf("*****5.展示联系人  0.  退出********\n");
	printf("***********************************\n");
}
int main()
{
	int op = -1;
	contact con;
	ContactInit(&con);
	//SLText01();
	//ContactText02();
	do {
		menu();
		printf("请选择您的操作:\n");
		scanf("%d", &op);
		switch(op)
		{
			case 1:
				ContactAdd(&con);
				break;
			case 2:
				ContactDel(&con);
				break;
			case 3:
				ContactModify(&con);
				break;
			case 4:
				ContactFind(&con);
				break;
			case 5:
				Contactshow(&con);
				break;
			case 0:
				printf("退出通讯录....\n");
				break;
			default:
				break;
		}
	} while (op != 0);
	ContactDestroy(&con);
	return 0;
}