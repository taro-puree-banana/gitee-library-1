#pragma once
#define Name_Max 20
#define Gender_Max 10
#define Tel_Max 20
#define Addr_Max 100
//定义联系人数据结构
//姓名 性别 年龄 电话 地址
typedef struct personInfo
{
	char name[Name_Max];
	char gender[Gender_Max];
	int age;
	char tel[Tel_Max];
	char addr[Addr_Max];
}person;

struct Seqlist;

typedef struct Seqlist contact;

//通讯录的初始化
void ContactInit(contact* con);
//通讯录的销毁
void ContactDestroy(contact* con);
//添加数据
void ContactAdd(contact* con);
//删除数据
void ContactDel(contact* con);
//修改数据
void ContactModify(contact* con);
//通讯录查找
void ContactFind(contact* con);
//展示通讯录数据
void Contactshow(contact* con);
