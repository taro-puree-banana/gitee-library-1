#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<assert.h>
//strcpy 字符函数的模拟实现（复制字符串）
char* my_strcpy(char* dest,const char* src)
{
	assert(dest != NULL);
	assert(src != NULL);
	char* ret = dest;
	while (*dest++ = *src++)
	{
		;
	}
	return ret;
}
int main()
{
	char arr1[] = "hello world";
	char arr2[20]="***************";
	char* ret=my_strcpy(arr2, arr1);
	printf("%s\n", ret);
	return 0;
}

//strcat 函数的模拟实现（连接两个字符串）
char* my_strcat(char* dest,const char* src)
{
	assert(dest && src);//判断不是空指针断言的第二种写法
	char* ret = dest;//存下数组首元素的地址
	//1.找到目标空间的\0
	while (*dest != '\0')
	{
		dest++;
	}
	//2.拷贝
	while (*dest++ = *src++)
	{
		;
	}
	return ret;//返回数组首元素的地址
}
int main()
{
	char arr1[20] = "hello ";
	char arr2[] = "world";
	char* ret=my_strcat(arr1, arr2);
	printf("%s\n", ret);
	return 0;
}

