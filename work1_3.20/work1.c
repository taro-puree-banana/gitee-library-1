#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
int Han(int arr[][3], int x, int y, int key)
{
	int i = 0;
	int j = y - 1;
	while (i < x && j >= 0)
	{
		if (arr[i][j] < key)
		{
			i++;
		}
		else if (arr[i][j] > key)
		{
			j--;
		}
		else
		{
			return 1;
		}
	}
	return 0;
}
int main()
{
	int arr[4][3] = { {1,2,3},{4,5,6},{7,8,9},{10,11,12} };
	int ret=Han(arr, 4, 3, 10);
	printf("%d", ret);
	return 0;
}