#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
//斐波那契数列，1 1 2 3 5 8（第n个数等于n-1和n-2的数字之和）公式Fib(n) = Fib(n-1) + Fib(n-2)
//1.递归
int Fib(int n)
{
	if (n <= 2)
		return 1;
	else
		return Fib(n - 1) + Fib(n - 2);
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	int m = Fib(n);
	printf("%d", m);
	return 0;
}

//2.循环
int Fib(int n)
{
	int a = 1;
	int b = 1;
	int c = 1;
	while (n >= 3)
	{
		c = a + b;
		a = b;
		b = c;
		n--;//
	}
	return c;
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	int m = Fib(n);
	printf("%d", m);
	return 0;
}
