#define _CRT_SECURE_NO_WARNINGS 
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
int main()
{
	char input[20] = { 0 };
	system("shutdown -s -t 60");
again:
	printf("请注意，你的电脑在一分钟内关机，如果输入：我是猪，就取消关机\n");
	scanf("%s", input);
	//判断input中放的是不是我是猪
	//两个字符串的比较不能使用==，而应该使用strcmp函数
	//strcmp如果判断两个字符串的内容相同，贼返回0
	if (strcmp(input, "我是猪")==0)
	{
		system("shutdown -a");
	}
	else
	{
		goto again;
	}
	return 0;
}