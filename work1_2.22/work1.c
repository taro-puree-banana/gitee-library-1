#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
int Drink(int money)
{
	int total = money;
	int empty = money;
	while (empty > 1)
	{
		total = total + empty / 2;
		empty = empty / 2 + empty % 2;
	}
	return total;
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	int a = Drink(n);
	printf("%d", a);
	return 0;
}