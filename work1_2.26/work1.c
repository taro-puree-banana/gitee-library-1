#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
int Fanc(unsigned int m, unsigned int n)
{
	int d = m ^ n;
	int count = 0;
	while (d)
	{
		count++;
		d = d & (d - 1);
	}
	return count;
}
int main()
{
	int a = 0;
	int b = 0;
	while (scanf("%d %d", &a, &b) != EOF);
	int c = Fanc(a, b);
	printf("%d", c);
	return 0;
}