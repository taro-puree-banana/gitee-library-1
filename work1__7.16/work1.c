#include<stdio.h>
struct ListNode
{
	int val;
	struct ListNode* next;
};
typedef struct ListNode ListNode;

struct ListNode* middleNode(struct ListNode* head)
{
	//创建快慢指针
	ListNode* slow = head;
	ListNode* fast = head;
	while (fast && fast->next)//不能交换位置
	{
		slow = slow->next;
		fast = fast->next->next;
	}
	//此时slow刚好指向中间节点
	return slow;
}