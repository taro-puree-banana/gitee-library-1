#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<math.h>
void Func()
{
	int i = 0;
	for (i = 0; i < 100000; i++)
	{
		//求当前数字i有几位数
		int count = 0;
		int temp = i;
		while (temp != 0)
		{
			count++;
			temp /= 10;
		}
		//求每一位的数字
		int tmp = i;
		int sum = 0;
		while (tmp != 0)
		{
			//pow(x,y)表示x的y次幂
			sum += pow(tmp % 10, count);
			tmp /= 10;
		}
		if (sum == i)
		{
			printf("%d\n",i);
		}
	}
}
int main()
{
	Func();
	return 0;
}