#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
//约瑟夫环：编号1-8的八个人按照1，2，3依次报数，报到3的人出局，输出出局的人的编号
//输入一个table[]前1-8个元素为1，下标为i，当j=3时，对应table[i]为0；注意i不能超过8；
int main()
{
	int table[100] = { 1 };
	int a = 0;
	for (a = 1; a <= 8; a++)
	{
		table[a] = 1;
	}
	int i = 0;//人数编号
	int j = 0;//1-3报数
	int count = 8;
	while (count > 0)
	{
		i++;
		j++;
		while (table[i] == 0)
		{
			i++;
			if (i > 8)
		{
			i = 1;
		}
		}
		
		if (j == 3)
		{
			table[i] = 0;
			printf("%d ", i);
			j = 0;
			count--;
		}
	}
	return 0;
}