#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
int Sum(int x, int y)
{
	return x + y;
}

int Jian(int x, int y)
{
	return x - y;
}

int Cheng(int x, int y)
{
	return x * y;
}

int Chu(int x, int y)
{
	return x / y;
}

void game(void)
{
	printf("*****************************\n");
	printf("*****1. Sum   2. Cheng*******\n");
	printf("*****3. Cheng 4. Chu  *******\n");
	printf("*****0. Exit           ******\n");
	printf("*****************************\n");
}
int main()
{
	int input;
	int x = 0;
	int y = 0;
	int (*p[5])(int,int) = {NULL,Sum,Jian,Cheng,Chu};
	do
	{
		game();
		printf("请选择：");
		scanf("%d", &input);
		if (input > 0 && input <= 4)
		{
			printf("请输入两个操作数：");
			scanf("%d%d", &x, &y);
			int ret = p[input](x, y);
			printf("%d\n", ret);
		}
		else if (input == 0)
		{
			printf("退出计算器\n");
			break;
		}
		else
		{
			printf("选择错误，请重新开始\n");
		}
	} while (input);
	return 0;
}