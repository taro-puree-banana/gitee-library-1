#include<stdio.h>
struct ListNode
{
	int val;
	struct ListNode* next;
};
typedef struct ListNode ListNode;

struct ListNode* removeElements(struct listNode* head, int val)
{
	ListNode* newHead, * newTail;//创建一个新链表
	newHead = newTail = NULL;
	ListNode* pcur = head;
	while (pcur)//遍历原链表
	{
		if (pcur->val != val)
		{
			//将原链表中不是目标元素的元素放入新链表中
			if (newHead == NULL)
			{
				//新链表为空
				newHead = newTail = pcur;
			}
			else
			{
				//新链表不为空，进行尾插
				newTail->next = pcur;
				newTail = newTail->next;
			}
		}
		pcur = pcur->next;
	}
	if(newTail)//若原链表为空，不能进行这步操作，所以要进行判断
	    newTail->next = NULL;
	return newHead;
}
