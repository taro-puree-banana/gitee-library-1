#include<stdio.h>
#include<string.h>
void alltrim(char* pstr, char* str)
{
	char* rstr;
	rstr = pstr + strlen(pstr) -1;
	while (*pstr == ' ')
	{
		pstr++;
	}
	while (rstr > pstr && *rstr == ' ')
	{
		rstr--;
	}
	while (pstr <= rstr)
	{
		*str++ = *pstr++;
	}
	*str++ = '\0';
}
int main()
{
	char* pstr, str[20];
	pstr = "   I have a good mood    ";
	alltrim(pstr, str);
	printf("%s", str);
	return 0;
}