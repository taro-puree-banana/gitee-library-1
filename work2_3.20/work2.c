#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
void Han(int arr[][4], int k)
{
	int i = 0;
	for (i = 0; i < k; i++)
	{
		int j = 0;
		for (j = 0; j <= i; j++)
		{
			if (i == j || j == 0)
			{
				arr[i][j] = 1;
			}
			else
			{
				arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
			}
		}
	}
}

void Printf(int arr[][4], int k)
{
	for (int i = 0; i < k; i++)
	{
		for (int j = 0; j <= i; j++)
		{
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
}
int main()
{
	int arr[4][4] = { 0 };
	Han(arr,4);
	Printf(arr, 4);
	return 0;
}