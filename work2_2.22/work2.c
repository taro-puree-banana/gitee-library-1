#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
void Func(int line)
{
	//打印上半部分
	int i = 0;
	for (i = 0; i < line; i++)
	{
		//先打印空格
		for (int j = 0; j < line - 1 - i; j++)
		{
			printf(" ");
		}
		//再打印*
		for (int j = 0; j < 2 * i + 1; j++)
		{
			printf("*");
		}
		printf("\n");
	}
	//打印下半部分
	for (int i = 0; i < line - 1; i++)
	{
		for (int j = 0; j < i + 1; j++)
		{
			printf(" ");
		}
		for (int j = 0; j < (line - 1 - i) * 2 - 1; j++)
		{
			printf("*");
		}
		printf("\n");
	}
}
int main()
{
	int n = 7;
	Func(n);
	return 0;
}