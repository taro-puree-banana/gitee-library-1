#include<stdio.h>
int main()
{
	int count = 0;
	int i = 0;
	for (i = 101; i <= 200; i += 2)
	{
		//判断i是否是素数
		//产生2~i-1之间的数字
		int flag = 1;//假设i是素数
		int a = 0;
		for (a = 2; a <= i - 1; a++)
		{
			if (i % a == 0)
			{
				flag = 0;//i不是素数
				break;
			}
		}
		if (flag == 1)
		{
			printf("%d ", i);
			count++;
		}
	}  
	printf("\ncount=%d\n", count);
	return 0;
}