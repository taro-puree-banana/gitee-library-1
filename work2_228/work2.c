#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
int Fanc(int n, int k)
{
	if (k == 1)
		return n;
	else
		return n * Fanc(n, k - 1);
}
int main()
{
	int a = 0;
	int b = 0;
	scanf("%d%d", &a, &b);
	int m = Fanc(a, b);
	printf("%d", m);
	return 0;
}