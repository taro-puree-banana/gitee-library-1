#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<math.h>
#include<time.h>

//clock():捕捉从程序开始运行到clock()被调用时所耗费的时间。这个时间单位是clock tick,即“时钟打点”
//常数CLK_TCK:机器时钟每秒所走的时钟打点数

clock_t start, stop;/*clock_t是clock()函数返回的变量类型*/
double duration;/*记录被测函数运行时间，以秒为单位*/

int main1()
{
	/*不在测试范围内的准备工作写在clock()调用函数之前*/
	start = clock();/*开始计时*/
	Manyfunction();/*被测函数加在这里*/
	stop = clock();/*停止计时*/
	duration = ((double)(stop - start)) / CLK_TCK;
	/*其他不在测试范围的处理写在后面，例如输出duration的值*/
	return 0;
}

//计算多项式的值，记录程序运行的时间
#define MAXN 10
#define ReMax 1e7

void Manyfunction1(int n, double a[], double x)
{
	int i = 0;
	double p = a[0];
	for (i = 0; i <= n; i++)
	{
		p += (a[i] * pow(x , i));
	}
}
void Manyfunction2(int n, double a[], double x)
{
	int i = 0;
	double p = a[n];
	for (i = n; i > 0; i--)
	{
		p = a[i - 1] + x * p;
	}
	
}
int main()
{
	double a[MAXN];//该数组用于存放每项的系数，MAXN是项数
	int i = 0;
	for (i = 0; i < MAXN; i++)
	{
		a[i] = i;
	}
	start = clock();
	for (i = 0; i < ReMax; i++)
	{
		Manyfunction2(MAXN - 1, a, 2);
	}
	duration = ((double)(stop - start)) / CLK_TCK;
	stop = clock();
	printf("ticks1=%f\n", (double)(stop - start));
	printf("duration=%6.2e\n", duration);
	return 0;
}