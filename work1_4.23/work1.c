#define _CRT_SECURE_NO_WARNINGS
//爬楼梯，小明爬楼梯，一步最多能上三个台阶，爬n个台阶，有多少种方法
// 1.递推式（小明一次能上的台阶数1||2||3），从最简单的情况开始推理
//只有一个台阶：1               =1种  F(1)
//只有两个台阶：11 + 2           =2种  F(2)
//只有三个台阶：111 +12 +21 +3     =4种  F(3)
//只有四个台阶：4(最后一步上一个台阶，说明前面上了三个台阶)+2(最后一步上了两个台阶)+1（最后一步上了三个台阶）
//             =F(3)+F(2)+F(1)种  F(4)
//只有五个楼梯：F(4)(最后一步上一个台阶)+F(3)(最后一步上了两个台阶)+F(2)（最后一步上了三个台阶）=F(5)
//结论，有n个楼梯：F(n)=F(n-1)+F(n-2)+F(n-3)
#include<stdio.h>
int Feb(int k)
{
	if (k == 1)
	{
		return 1;
	}
	else if(k==2)
	{
		return 2;
	}
	else if (k == 3)
	{
		return 4;
	}
	else
	{
		return Feb(k - 1) + Feb(k - 2) + Feb(k - 3);
	}
}
int main()
{
	int k = 0;
	scanf("%d", &k);
	int ret=Feb(k);
	printf("%d\n", ret);
	return 0;
}