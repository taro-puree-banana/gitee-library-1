#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
int main()
{
	int year = 0;
	
	int mouth = 0;
	while (scanf("%d%d", &year, &mouth) != EOF)
	{
		int input = 0;
		if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0))
		{
			input = 1;
		}
		switch (mouth)
		{
		case 1: case 3: case 5:case 7: case 8: case 10: case 12:
			printf("31\n");
			break; 
		case 4: case 6: case 9: case 11:
			printf("30\n");
			break;
		case 2:
			if (input)
			{
				printf("29\n");
			}
			else
			{
				printf("28\n");
			}
			break;
		default:
			break;
		}
	}
	
	return 0;
}