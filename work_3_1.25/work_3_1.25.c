#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
//闰年判断的规则
//1.能被4整除，并且不能被100整除的是闰年
//2.能被400整除的是闰年
int main()
{
	int year = 0;
	for (year = 1000; year <= 2000; year++)
		if ((year % 4 == 0) && (year % 100 != 0))
			printf("%d ", year);
		else if (year % 400 == 0)
		printf("%d ", year);
	return 0;
}