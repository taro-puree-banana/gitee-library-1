#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
int bin_search(int arr[], int left, int right, int key)
{
	int mid = 0;
	while (left <= right)
	{
		mid = (left + right) / 2;
		if (arr[mid] < key)
		{
			left = mid + 1;
		}
		else if (arr[mid] > key)
		{
			right = mid - 1;
		}
		else
		{
			return mid;
		}
	}
	return 0;
}
int main()
{
	int key = 0;
	int arr[7] = { 1,3,5,6,8,9,10 };
	int left = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	int right = sz - 1;
	scanf("%d", &key);
	int b = bin_search(arr, left, right, key);
	if (b)
	{
		printf("找到了，下标是%d", b);
	}
	else
	{
		printf("没找到，返回-1");
	}
	return 0;
}